import pygame
import sys
import random
import time
# Vier Bälle, die ohne Dämpfung und Kollisionserlennung an den 4 Rändern abprallen

pygame.init()

size = width, height = 1000, 800

black = 0, 0, 0  # r, g, b
counter = 1

screen = pygame.display.set_mode(size)

speeds = []
speedsdyn = []
balls = []
ballrects = []
numBalls = 4
justTouched = []

for i in range(0, numBalls):
    ball = pygame.image.load("intro_ball.gif")
    ballrect = ball.get_rect()
    ballrect = ballrect.move([50 * i, 20 * i])
    balls.append(ball)
    ballrects.append(ballrect)
    speeds.append([random.randint(1, 10), random.randint(1, 10)])
    speeds.append([1, 1])
    justTouched.append(False)
jetzt = int(time.time())
while True:
    print(time.time())
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    screen.fill(black)

    for i in range(0, numBalls):
        ballrect = ballrects[i].move(speeds[i])

        if int(time.time()) - jetzt > 1:
            speeds[i][0] += 1
            jetzt = int(time.time())


        if ballrect.left < 0 or ballrect.right > width:
            speeds[i][0] = -speeds[i][0]

        if ballrect.top < 0 or ballrect.bottom > height:
            speeds[i][1] = -speeds[i][1]
            #counter = 1

        screen.blit(balls[i], ballrect)
        ballrects[i] = ballrect

    for i in range(0, numBalls):
        list = ballrects[i].collidelistall(ballrects)
        if len(list) > 1 and justTouched[i] == False:
            speeds[i][0] = -speeds[i][0]  # have to be improved
            speeds[i][1] = -speeds[i][1]  # have to be improved
            justTouched[i] = True
        elif len(list) <= 1:
            justTouched[i] = False

    counter += 1
    pygame.display.flip()
    print(speeds)
# Erdbeschläunigung hinzufügen
# andere Bälle ergänzen
