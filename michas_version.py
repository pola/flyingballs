import pygame
import sys
import random

# Vier Bälle, die ohne Dämpfung und Kollisionserlennung an den 4 Rändern abprallen

pygame.init()

size = width, height = 1400, 1000

black = 0, 0, 0

screen = pygame.display.set_mode(size)

speeds = []
balls = []
ballrects = []
numBalls = 4
justTouched = []

for i in range(0, numBalls):
    ball = pygame.image.load("intro_ball.gif")
    ballrect = ball.get_rect()
    ballrect = ballrect.move([50 * i, 20 * i])
    balls.append(ball)
    ballrects.append(ballrect)
    speeds.append([random.randint(1, 10), random.randint(1, 10)])
    justTouched.append(False)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    screen.fill(black)

    for i in range(0, numBalls):
        ballrect = ballrects[i].move(speeds[i])
        if ballrect.left < 0 or ballrect.right > width:
            speeds[i][0] = -speeds[i][0]

        if ballrect.top < 0 or ballrect.bottom > height:
            speeds[i][1] = -speeds[i][1]

        screen.blit(balls[i], ballrect)
        ballrects[i] = ballrect

    for i in range(0, numBalls):
        list = ballrects[i].collidelistall(ballrects)
        if len(list) > 1 and justTouched[i] == False:
            speeds[i][0] = -speeds[i][0] # have to be improved
            speeds[i][1] = -speeds[i][1] # have to be improved
            justTouched[i] = True
        elif len(list) <=1:
            justTouched[i] = False

    pygame.display.flip()
